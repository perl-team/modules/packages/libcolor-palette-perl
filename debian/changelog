libcolor-palette-perl (0.100004-1) unstable; urgency=medium

  * Import upstream version 0.100004.
  * Update years of upstream copyright.
  * Update upstream email address.
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Jan 2023 23:07:21 +0100

libcolor-palette-perl (0.100003-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Drop xz compression for {binary,source} package, set by default by
    dpkg since 1.17.{0,6}.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 11 Jun 2022 22:59:10 +0100

libcolor-palette-perl (0.100003-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 11:41:50 +0100

libcolor-palette-perl (0.100003-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Dec 2013 21:00:19 +0100

libcolor-palette-perl (0.100002-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * Add a Pre-Depends on dpkg (>= 1.15.6~) for the xz compression.

  [ Ansgar Burchardt ]
  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 20 Dec 2011 23:34:46 +0100

libcolor-palette-perl (0.100000-1) unstable; urgency=low

  * Team upload.

  [ Fabrizio Regalli ]
  * Bump to 3.9.2 Standard-Version.
  * Add myself to Uploaders and Copyright.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).
  * Bump to 3.0 quilt format.
  * Updated Format-Specification link in d/copyright.
  * Fixed lintian copyright-refers-to-symlink-license message.

  [ Ansgar Burchardt ]
  * New upstream release.
  * debian/copyright: Update years of copyright; refer to "Debian systems"
    instead of "Debian GNU/Linux systems".
  * Use XZ compression for source and binary packages.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 19 Oct 2011 17:01:50 +0200

libcolor-palette-perl (0.091400-1) unstable; urgency=low

  * Initial Release (Closes: #566903)

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 01 Feb 2010 23:11:13 -0500
